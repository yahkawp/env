Env - my work environment
=========================


Read [sources](htmlsrc)

Releases in [archive](archive)


[ hashpass ](site/hashpass.html)

### fish shell


    ls | while read -l f  ;  echo $f; end
or
    for i in *; echo $i ; end 
filecheck
    if -f notepad.txt ; echo "jsdf"; end⏎


### perl

* remove all newlines

    $line =~ s/\R//g;

or before 5.10

    $line =~ s/\015?\012?$//;


* file slurp 

    open(my $f, '<', $filename) or die "Err $filename: $!\n";
    $string = do { local($/); <$f> };
    close($f);


### markdown

[md](rsc/markdown/markdown.md)
[txt](rsc/markdown/markdown.txt)
[html](rsc/markdown/markdown/index.html)


## NOTEPAD

### rsync

    rsync -avz -e "ssh" . srctxt.org:

    - "srctxt.org" is in ~/.ssh/config
    - don't forget the colon
    - a: archive, z: compress, v: verbose

## chiselapp

    Anonymous Cloning: $ fossil clone https://chiselapp.com/user/ykwp/repository/env env.fsl
    Authenticated Cloning: $ fossil clone https://ykwp@chiselapp.com/user/ykwp/repository/env env.fsl


## fossil 

    fossil settings ignore-glob "*.o,*.obj,*.exe,*/fishd.*,*/*_history " --global


## ssh / gpg

    password for ssh keys are cached with [keychain](http://www.funtoo.org/Keychain)



## ubuntu

localization, change language etc: 

    sudo vim /etc/default/locale 

## firefox

disable super annoying mouse wheel zooming

    > about:config
    > mousewheel.with_meta.action 
    > 0

set url for new tab
    > about:config
    > browser.newtab.url


## date

date "+%y.%m.d%"

## tricks

which process runs on port

    sudo netstat -lpn | grep :8080

untar tar.xz file
    tar -xJf file.pkg.tar.xz

change password of ssh key

     ssh-keygen -f id_dsa -p


## Filename & Extension

    filename=$(basename "$fullfile")
    extension="${filename##*.}"
    filename="${filename%.*}"


## Docker

    # du -sh /var/lib/docker/*
        695M    /var/lib/docker/aufs
        628K    /var/lib/docker/containers
        80K     /var/lib/docker/execdriver
        500K    /var/lib/docker/graph
        24M     /var/lib/docker/init
        12K     /var/lib/docker/linkgraph.db
        4.0K    /var/lib/docker/repositories-aufs
        4.0K    /var/lib/docker/tmp
        8.0K    /var/lib/docker/trust
        4.0K    /var/lib/docker/volumes


