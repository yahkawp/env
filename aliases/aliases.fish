alias l 'ls'
alias cx 'chmod +x'

alias .. 'cd ..'
alias cd.. 'cd ..'

function ass; apt-cache search $argv | grep -wi $argv ; end

alias apti 'sudo apt-get install'

function el; elinks -remote "openURL($argv)"; end

function serv; python -m SimpleHTTPServer 8000 $argv & ; end
