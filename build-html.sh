#!/bin/sh


rm -rf html 

mkdir -p html


find . -type f  | while read f; do
    dir=$(dirname $f)
    name=$(basename $f)
    [ -d "html/$dir" ] || mkdir -p html/$dir
    { 
        echo "<html><head><title>$name</title></head><body><pre>"
        cat $f
        echo "</pre></body></html>"
    } > html/$dir/$name.html
done

