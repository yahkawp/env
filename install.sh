#!/bin/sh

favs=$HOME/favs
reset=$1

[ -e "$favs" ] || { echo "Err: no favs folder for $favs" ;  exit 1; } 

bins=$HOME/bins/
[ -n "$reset" ] && rm -rf $bins

[ -d "$bins/links" ] || mkdir -p $bins/links



cwd=$(pwd)

rm -f ~/.memos
ln -s $cwd/memos ~/.memos

rm -f $favs/env
ln -s $cwd $favs/env

rm -f $favs/dotfiles
ln -s $cwd/dotfiles $favs/dotfiles


# sbin
rm -f $bins/sbin
ln -s $cwd/sbin $bins/sbin

# utils
rm -f $bins/tools
ln -s $cwd/tools $bins/tools


rm -f ~/.notepad.txt
ln -s $cwd/notepad.txt ~/.notepad.txt

for a in aliases/*; do
    [ -f $a ] || continue
    b=$(basename $a)
    rm -f $HOME/.$b
    ln -s $cwd/$a $HOME/.$b
done
