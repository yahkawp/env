#!/usr/bin/perl
#
use Digest::SHA qw(sha256_base64);

use Term::ReadKey;

$passtab='./passtab/passtab-0.0.2/bin/passtab';
$passtab_db='/home/ben/.mastertable.json';

die "Err: no rectula $passtab_db" unless -f $passtab_db;

print "coord 1\n";
ReadMode('noecho'); 
chomp(my $coord1 = <STDIN>);

print "coord 2\n";
ReadMode('noecho'); 
chomp(my $coord2 = <STDIN>);

print "coord 3\n";
ReadMode('noecho'); 
chomp(my $coord3 = <STDIN>);

ReadMode(0);        # back to normal

print "ok ... \n";

($n, $d) = split /\:/, $coord2;

$nn = $n;
$nn--;

$hash = sha256_base64("$coord1#$n:$d#$coord3");


$ctrl = q(UcaiptTNLF83BbLg/ASOaSQD1OxLqXQz9d4NcriOyF0);

die "Err: wrong pw for " unless $hash eq $ctrl;

my ($res) =  qx($passtab -i $passtab_db -g '$coord1' -s '$nn:$d' --collision '$coord3' );




chomp $res;

system(" echo -n '$res' | xclip -sel clip ");
