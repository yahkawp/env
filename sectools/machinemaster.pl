#!/usr/bin/perl
#
use strict;
use Digest::SHA qw(sha256_base64);

use Term::ReadKey;
use IO::Socket::INET;

my $passtab='./passtab/passtab-0.0.2/bin/passtab';
my $passtab_db= $ENV{HOME} . '/.mastertable.json';

my $addr = '127.0.0.1';
my $port = '7777';

$| = 1;
my $socket = new IO::Socket::INET 
    ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

my $response;
my $has_server;
if ($socket){

    $has_server = 1;
     
    my $req = 'get -> machinedata';
    my $size = $socket->send($req);
    shutdown($socket, 1);
    $response = "";
    $socket->recv($response, 1024);
    $socket->close();
}

if($ARGV[0] eq 'reset'){
    $response = undef;
}
my ($passw,  $rectula, $pwlength, $passcoord, $machcoord) ;
if ($response){
    ($passw) = $response;
    
}else{
    die "Err: no webpasstab_db file" unless -f  $passtab_db;

    print "coord 1\n";
    ReadMode('noecho'); 
    chomp( my $_coord1 = <STDIN>);

    print "coord 2\n";
    chomp(my $_coord2 = <STDIN>);


    print "coord 3\n";
    chomp(my $_coord3 = <STDIN>);

    ReadMode(0);        # back to normal

    print "mach symbol \n";
    chomp( my $machsymbol = <STDIN>);

    print "variant \n";
    my $variant = 0;
    chomp( $variant = <STDIN>);


    print "ok ... \n";

    my @coords1 = split //, $_coord1;
    my @coords2 = split //, $_coord2;
    my @coords3 = split //, $_coord3;



    $passcoord = $_coord1 . $_coord2 . $_coord3;
    my $coord1 = join ':', ($_coord1 , $machsymbol);
    my $coord3 = join ',', @coords3;

    my $hash = sha256_base64($passcoord);

    my $ctrl = q(JXh3LCe48a644ExWuq9KDGtGKg7rqacLNAntC8UTXxU);

    die "Err: wrong pw for " unless $hash eq $ctrl;


    my ($c1) = $coords2[0] * 10;
    my $n = $coords2[1] + $c1;
    my ($d) = $coords2[2];
    my $nn = $n + $variant  ;
    $pwlength = $nn;
    $nn--;
    ($rectula) =  qx($passtab -i $passtab_db -g '$coord1' -s '$nn:$d' --collision '$coord3' );
    chomp $rectula;

    $machcoord = $_coord1 . $machsymbol . $pwlength . $d .  $_coord3;
    $passw = "$machcoord.$rectula";

}


if($has_server){
    my $req = "set -> machinedata -> $passw";
    
    my $sock = new IO::Socket::INET 
        ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

    die "cannot connect o the server $!\n" unless $sock;
    my $size = $sock->send($req);
    shutdown($sock, 1);
    my $response = "";
    $sock->recv($response, 1024);
    $sock->close();
}



#print "subpw:" . $cres . "\n";
system(" echo -n '$passw' | xclip -sel clip ");

