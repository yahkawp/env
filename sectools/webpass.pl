#!/usr/bin/perl
#
use strict;
use Digest::SHA qw(sha256_base64);

use Term::ReadKey;
use IO::Socket::INET;

my $passtab='./passtab/passtab-0.0.2/bin/passtab';
my $passtab_db= $ENV{HOME} . '/.webtable.json';

my $addr = '127.0.0.1';
my $port = '7777';

$| = 1;
my $socket = new IO::Socket::INET 
    ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

my $response;
my $has_server;
if ($socket){

    $has_server = 1;
     
    my $req = 'get -> webdata';
    my $size = $socket->send($req);
    shutdown($socket, 1);
    $response = "";
    $socket->recv($response, 1024);
    $socket->close();
}

if($ARGV[0] eq 'reset'){
    $response = undef;
}
my ($passw, $rectula, $pwlength, $dir, $passcoord , $suffix) ;
if ($response){
    ($rectula, $pwlength, $passcoord, $dir, $suffix ) = split /\#\#\#\#/, $response;
    
    unless ($dir){
        print "dir: \n";
        chomp($dir= <STDIN>);

    }
    unless ($suffix){
        print "Suffix: \n";
        chomp($suffix = <STDIN>);

    }
}else{
    print "coord 1\n";
    ReadMode('noecho'); 
    chomp( my $_coord1 = <STDIN>);

    print "coord 2\n";
    chomp(my $_coord2 = <STDIN>);

    print "coord 3\n";
    chomp(my $_coord3 = <STDIN>);

    print "Suffix: \n";
    chomp($suffix = <STDIN>);

    ReadMode(0);        # back to normal

    print "ok ... \n";

    my @coords1 = split //, $_coord1;
    my @coords2 = split //, $_coord2;
    my @coords3 = split //, $_coord3;


    my $coord1 = join ':', @coords1;
    my $coord2 = join ':', @coords2;
    my $coord3 = join ',', @coords3;

    $passcoord = $_coord1 . $_coord2 . $_coord3;

    my $hash = sha256_base64($passcoord . $suffix);

    my $ctrl = q(2K87C/xGLBMXaAWuQBEugg1ZhtzCju1kPlNz8qJ4G64);

    die "Err: wrong pw for " unless $hash eq $ctrl;
    die "Err: no webpasstab_db file" unless -f  $passtab_db;

    my ($c1) = $coords2[0] * 10;
    my $n = $coords2[1] + $c1;
    my ($d) = $coords2[2];
    my $nn = $n  ;
    $pwlength = $n;
    $nn--;
    ($rectula) =  qx($passtab -i $passtab_db -g '$coord1' -s '$nn:$d' --collision '$coord3' );
    chomp $rectula;

    print "Dir: \n";
    chomp($dir = <STDIN>);
}

my $passw = $rectula . '####' . $pwlength . '####' . $passcoord . '####' . $dir . '####' . $suffix;

if($has_server){
    my $req = "set -> webdata -> $passw";
    
    my $sock = new IO::Socket::INET 
        ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

    die "cannot connect o the server $!\n" unless $sock;
    my $size = $sock->send($req);
    shutdown($sock, 1);
    my $response = "";
    $sock->recv($response, 1024);
    $sock->close();
}


my $salt;
if($ARGV[0]){
    unless ($ARGV[0] eq 'reset'){
        $salt = $ARGV[0]
    }
}
unless ($salt){
    print "Salt: \n";
    chomp($salt = <STDIN>);
}

my $pw = sha256_base64("$dir.$rectula:$salt");

my $s = $pwlength / 4;
my $hashl = $s * 6;
my $epw = substr($pw, 0, $hashl);


my $final = $epw . $suffix;

#print "subpw:" . $cres . "\n";
system(" echo -n '$final' | xclip -sel clip ");

