use strict;
use warnings;
use IO::Socket::INET;
 
# auto-flush on socket
$| = 1;
 
# creating a listening socket
my $socket = new IO::Socket::INET (
    LocalHost => '0.0.0.0',
    LocalPort => '7777',
    Proto => 'tcp',
    Listen => 5,
    Reuse => 1
);
die "cannot create socket $!\n" unless $socket;
print "server waiting for client connection on port 7777\n";

my %dict = ();
 

sub fetch {
    my ($k) = @_;
    return $dict{$k};
}
sub store{
    my ($k, $v ) = @_;
    $dict{$k} = $v;
    return ""
}
my %dispatch_case = (
    get =>  \&fetch,
    set => \&store,
    _ => sub { die "Err: invalid request with $_[0]" }

);
sub dispatch {
    my ($str ) = @_;

    my ($dir, $k, $v) = split /\s*\-\>\s*/ , $str;
    chomp $dir;
    
    return (exists $dispatch_case{$dir})
        ? $dispatch_case{$dir}->($k, $v)
        : $dispatch_case{_}->($dir);
    
}
while(1)
{
    # waiting for a new client connection
    my $client_socket = $socket->accept();
 
    # get information about a newly connected client
    my $client_address = $client_socket->peerhost();
    my $client_port = $client_socket->peerport();
    print "connection from $client_address:$client_port\n";
 
    # read up to 1024 characters from the connected client
    my $data = "";
    $client_socket->recv($data, 1024);
    my $resp = dispatch($data);
 
    $client_socket->send($resp);
 
    # notify client that response has been sent
    shutdown($client_socket, 1);
}
 
$socket->close();
