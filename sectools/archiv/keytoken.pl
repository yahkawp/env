#!/usr/bin/perl
#
use strict;
use Digest::SHA qw(sha256_base64);

use Term::ReadKey;
use IO::Socket::INET;

my $passtab='./passtab/passtab-0.0.2/bin/passtab';
my $passtab_db= $ENV{HOME} . '/.mastertable.json';

my $addr = '127.0.0.1';
my $port = '7777';

$| = 1;
my $socket = new IO::Socket::INET 
    ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

my $response;
my $has_server;
if ($socket){

    $has_server = 1;
     
    my $req = 'get -> keydata';
    my $size = $socket->send($req);
    shutdown($socket, 1);
    $response = "";
    $socket->recv($response, 1024);
}



if($ARGV[0] eq 'reset'){
    $response = undef;
}


my ($secstring, $coord1, $coord2, $coord3, $pwlength, $mastertoken , $rectula , $mach, $vers) ;
if ($response){
    ( $secstring, $rectula , $pwlength, $mach, $vers )= split /\#\#\#/, $response;
}else{
    print "coord 1\n";
    ReadMode('noecho'); 
    chomp( my $_coord1 = <STDIN>);

    print "coord 2\n";
    chomp(my $_coord2 = <STDIN>); 
    print "coord 3\n";
    chomp(my $_coord3 = <STDIN>);

    ReadMode(0);        # back to normal

    print "ok ... \n";

    my @coords1 = split //, $_coord1;
    my @coords2 = split //, $_coord2;
    my @coords3 = split //, $_coord3;


    my $coord1 = join ':', @coords1;
    my $coord2 = join ':', @coords2;
    my $coord3 = join ',', @coords3;

    my $passcoord = "$coord1#$coord2#$coord3";

    my $hash = sha256_base64($passcoord);

    my $ctrl = q(vxX+dilyyBLfxdumFcQeUVfDstAvvX6k0U84eqbZfXc);


    die "Err: wrong pw for " unless $hash eq $ctrl;

    print "mach: \n";
    chomp($mach = <STDIN>);

    print "vers: \n";
    chomp($vers = <STDIN>);

    unless ($vers) { $vers = 0 };

    die "Err: no keypasstab_db file" unless -f  $passtab_db;
    my ($n, $d) = split /\:/, $coord2;
    my $nn = $n * 4 ;
    $pwlength = $nn;
    $nn--;
    $secstring = $_coord1 . $_coord2 . $_coord3;
    ($rectula) =  qx($passtab -i $passtab_db -g '$coord1' -s '$nn:$d' --collision '$coord3' );
    chomp $rectula;
    my $passw = $secstring . '###' . $rectula . '###' . $pwlength . '###' . $mach . '###' .  $vers ;

    if($has_server){
        my $req = "set -> keydata -> $passw";
        
        my $sock = new IO::Socket::INET 
            ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

        die "cannot connect o the server $!\n" unless $sock;
        my $size = $sock->send($req);
        shutdown($sock, 1);
        my $response = "";
        $sock->recv($response, 1024);
        $socket->close();
    }
}


my $pw = "$secstring.$rectula";

#print "subpw:" . $cres . "\n";
system(" echo -n '$pw' | xclip -sel clip ");

