#!/usr/bin/perl
#
use strict;
use Digest::SHA qw(sha256_base64);

use Term::ReadKey;
use IO::Socket::INET;


my $addr = '127.0.0.1';
my $port = '7777';

$| = 1;
my $socket = new IO::Socket::INET 
    ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

my $response;
my $has_server;
if ($socket){

    $has_server = 1;
     
    my $req = 'get -> webhash';
    my $size = $socket->send($req);
    shutdown($socket, 1);
    $response = "";
    $socket->recv($response, 1024);
    $socket->close();
}

if($ARGV[0] eq 'reset'){
    $response = undef;
}
my ($passw, $pwlength, $dir, $passphrase , $suffix) ;
if ($response){
    ($pwlength, $dir, $passphrase, $suffix ) = split /\#\#\#\#/, $response;
    
    ReadMode('noecho'); 
    unless ($pwlength){
        print "pwlength: \n";
        chomp($pwlength = <STDIN>);

    }
    unless ($dir){
        print "dir: \n";
        chomp($dir= <STDIN>);

    }
    unless ($passphrase){
        print "pwlength: \n";
        chomp($passphrase = <STDIN>);

    }
    unless ($suffix){
        print "Suffix: \n";
        chomp($suffix = <STDIN>);

    }
    ReadMode(0);        # back to normal
}else{
    ReadMode('noecho'); 

    print "pwlength: \n";
    chomp($pwlength = <STDIN>);


    print "passphrase: \n";
    chomp($passphrase = <STDIN>);

    print "Suffix: \n";
    chomp($suffix = <STDIN>);

    ReadMode(0);        # back to normal

    print "ok ... \n";

    my $pass_check = $passphrase . $suffix;
    my $hash = sha256_base64($pass_check);


    my $ctrl = q(30xfRCehLyrofP+/J9Yiv7lBBfQ2OK+Xfpjn78ii8GU);

    die "Err: wrong pw for " unless $hash eq $ctrl;

    print "Dir: \n";
    chomp($dir = <STDIN>);
}

my $pass_coord = $pwlength . '####' . $passphrase . '####' . $dir . '####' . $suffix;

if($has_server){
    my $req = "set -> webhash -> $pass_coord";
    
    my $sock = new IO::Socket::INET 
        ( PeerHost => $addr, PeerPort => $port, Proto => 'tcp',);

    die "cannot connect o the server $!\n" unless $sock;
    my $size = $sock->send($req);
    shutdown($sock, 1);
    my $response = "";
    $sock->recv($response, 1024);
    $sock->close();
}


my $salt;
if($ARGV[0]){
    unless ($ARGV[0] eq 'reset'){
        $salt = $ARGV[0]
    }
}
unless ($salt){
    print "Salt: \n";
    chomp($salt = <STDIN>);
}

my $pw = sha256_base64("$dir.$passphrase:$salt");

my $s = $pwlength - 2;
my $epw = substr($pw, 0, $s);


my $final = $epw . $suffix;

#print "subpw:" . $cres . "\n";
system(" echo -n '$final' | xclip -sel clip ");

