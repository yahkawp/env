#!/bin/sh

cwd=$(pwd)

trash=$HOME/trash
[ -d "$trash" ] || mkdir $trash

tools=$HOME/tools

[ -d $tools ] || mkdir -p $tools

rm -f $tools/dotbin
ln -s bin_dir $tools/dotbin

for f in *; do
	if [ -d "$f" ];  then

	case $f in
	*_files) 
		for ff in $f/*; do
			[ -e "$ff" ] || continue
			bn=$(basename $ff)
            		[ -z $bn ] && continue
			t="$trash"/$(date +"%Y%m%d%M%S")
			[ -d "$t" ] || mkdir -p  $t
			[ -e "$HOME/.$bn" ] && { mv $HOME/.$bn $t/ ; }
			rm -rf $HOME/.$bn
			ln -s 	$cwd/$ff $HOME/.$bn
		done
	;;
	*_dir) 
		bn=$(basename $f)
        	name=${bn%_*}
		rm -rf $HOME/.$name
        ln -s $cwd/$f $HOME/.$name
	;;
	*_sys)  # non destructive sub folders
		bn=$(basename $f)
        	name=${bn%_*}
		sdir=$HOME/.$name
		[ -e "$sdir" ] || mkdir $sdir 
        for ff in $f/*; do
            bff=$(basename $ff)
            [ -e "$ff" ] || continue
            rm -rf  $sdir/$bff
      ln -s 	$cwd/$ff $sdir/$bff
        done
	;;
	*_csys)  # non destructive sub folders under ~/.config
		bn=$(basename $f)
        	name=${bn%_*}
		sdir=$HOME/.config/$name
		[ -e "$sdir" ] || mkdir $sdir 
        for ff in $f/*; do
            bff=$(basename $ff)
            [ -e "$ff" ] || continue
            rm -rf  $sdir/$bff
      ln -s 	$cwd/$ff $sdir/$bff
        done
	;;
	*)
		n=0
#i		[ -e "$HOME/.$f" ] && { mv $HOME/.$f $t/ ; }
#		rm -rf $HOME/.$f 
 #       	ln -s 	$cwd/$f $HOME/.$f
	;;
	esac
else
        [ "$f" = 'install.sh' ] && continue
        [ "$f" = 'README.txt' ] && continue
		rm -f $HOME/.$f
		ln -s $cwd/$f $HOME/.$f

fi
done
