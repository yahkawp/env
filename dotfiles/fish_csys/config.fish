fish_vi_mode
set -g SHELL fish
[ -f $HOME/.aliases.fish ] ; and . ~/.aliases.fish


# keychain (ssh, gpg)
#  keychain --eval --quiet -Q id_rsa yahkawp_rsa

set -gx HOSTNAME (hostname)
#if status --is-interactive;
    #keychain --nogui --clear 
#  keychain --eval   --quiet -Q ~/.ssh/yahkawp_acer.rsa 35827ADE
  keychain --quiet --nogui ~/.ssh/yahkawp_acer.rsa 35827ADE

  [ -e $HOME/.keychain/$HOSTNAME-fish ]; and . $HOME/.keychain/$HOSTNAME-fish
  [ -e $HOME/.keychain/$HOSTNAME-fish-gpg ]; and . $HOME/.keychain/$HOSTNAME-fish-gpg

#end

set bins $HOME/bins

# PATH
test -d "/bin"; and set PATH "/bin"
test -d "/sbin"; and set PATH "/sbin" $PATH
test -d "/usr/bin"; and set PATH "/usr/bin" $PATH
test -d "/usr/sbin"; and set PATH "/usr/sbin" $PATH
test -d "/usr/local/bin"; and set PATH "/usr/local/bin" $PATH
test -d "/usr/local/sbin"; and set PATH "/usr/local/sbin" $PATH

test -d "$HOME/local/bin"; and set PATH "$HOME/local/bin" $PATH

test -d "$bins/links"; and set PATH "$bins/links" $PATH
test -d "$bins/sbin"; and set PATH "$bins/sbin" $PATH
test -d "$bins/tools"; and set PATH "$bins/tools" $PATH
test -d "$bins/sectools"; and set PATH "$bins/sectools" $PATH

# GLOBALS
set -g -x VISUAL vim
set -g -x EDITOR vim
set -g -x PAGER less
set -g -x DOCHOME ~/docs

alias l 'ls'


# Fish editing
function ef; vim ~/.fish/config.fish; end
function rf; . ~/.fish/config.fish; end


